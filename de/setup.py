"""
essential development and setup helper functions
================================================

this core portion of the 'de' namespace is providing helper functions for the development and the setup of namespace
root packages and their namespace portions.


basic helper functions
----------------------

use :func:`file_content` to read e.g. the content of README file of your portion project, to provide it to the
`long_description` kwarg of `setuptools.setup`.

while :func:`code_file_version` determines the current version of any type of Python code file, :func:`code_file_title`
does the same for the title of the code file's docstring.

the :func:`setup_kwargs` ensures that this module will be included in the `install_requires` and `setup_requires`
options/kwargs of the :meth:`setuptools.setup` method (called by ``setup.py``).


namespace environment variables
-------------------------------

the function :func:`namespace_env_vars` returns a complete mapping of the properties (names, urls, file paths, version,
...) of a namespace portion, the namespace root package or an application project::

    nev = namespace_env_vars()

by adding this function call, e.g. at the top of the `setup.py` file of your namespace portion project, you can easily
access the properties from the `nev` variable, either via :func:`nev_str`, :func:`nev_val` or directly via getitem. the
following example is retrieving a string property reflecting the package name of a portion::

    package_name = nev_str(nev, 'package_name')

other useful properties in the `nev` mapping dictionary are e.g. `package_version` (determined e.g. from the __version__
variable of the project) or `repo_root` (the url prefix to the remote/origin portion/root repositories). for a
complete list check the returned `nev` variable or the code in the function :func:`namespace_env_vars`.

.. hint::
    the namespace environment variable values can be adopted either by patching the module variables of this package,
    or by :mod:`setting up a setup hook <de.setup_hook>`.

"""
import glob
import os
import re
import subprocess
from typing import Any, Dict, List, Optional, Sequence, Tuple, Union


__version__ = '0.2.1'


NevVarType = Union[str, List[str], Tuple[str, ...]]
NevType = Dict[str, NevVarType]


PORTIONS_COMMON_DIR = 'portions_common_root'            #: default folder in root package for portions common files

APP_PRJ: str = 'app'                                    #: application project type
MODULE_PRJ: str = 'module'                              #: module portion/project type
PACKAGE_PRJ: str = 'sub-package'                        #: sub-package portion/project type
ROOT_PRJ: str = 'namespace-root'                        #: namespace root project type

PY_EXT = '.py'                                          #: default file extension for portions and hooks

REPO_CODE_DOMAIN = 'gitlab.com'                         #: origin code repository internet/dns domain
REPO_PAGES_DOMAIN = 'gitlab.io'                         #: origin repository pages internet/dns domain
REPO_GROUP_SUFFIX = '-group'                            #: origin repository users group name suffix

REQ_FILE_NAME = 'requirements.txt'                      #: default file name for all/dev requirements
REQ_TEST_FILE_NAME = 'test_requirements.txt'            #: default file name for test requirements

TESTS_FOLDER = 'tests'                                  #: folder name to store unit/integration tests

VERSION_QUOTE = "'"
VERSION_PREFIX = "__version__ = " + VERSION_QUOTE


def code_file_title(file_name: str) -> str:
    """ determine docstring title of a Python code file.

    :param file_name:           name (and optional path) of module/script file to read the docstring title number from.
    :return:                    docstring title string or empty string if file|docstring-title not found.
    """
    try:
        lines = file_content(file_name).split('\n')
        title = lines[0][3:].strip() or lines[1].strip()
    except (FileNotFoundError, IndexError, OSError):
        title = ""
    return title


def code_file_version(file_name: str) -> str:
    """ read version of Python code file - from __version__ module variable initialization.

    :param file_name:           name (and optional path) of module/script file to read the version number from.
    :return:                    version number string or empty string if file or version-in-file not found.
    """
    try:
        content = file_content(file_name)
        version_match = re.search("^" + VERSION_PREFIX + "([^" + VERSION_QUOTE + "]*)" + VERSION_QUOTE, content, re.M)
    except (FileNotFoundError, OSError):
        version_match = None
    return version_match.group(1) if version_match else ""


def file_content(file_name: str) -> str:
    """ returning content of the file specified by file_name arg as string.

    :param file_name:           file name to load into a string.
    :return:                    file content string.
    """
    with open(file_name) as file_handle:
        return file_handle.read()


def find_package_data(namespace_name: str, portion_name: str) -> List[str]:
    """ find kv lang files, i18n translation texts, images and sound resource files of package portion.

    :param namespace_name:      name space name/id.
    :param portion_name:        portion name.
    :return:                    list of resource files to be used in the setup package_data kwarg value.
    """
    pgk_path = os.path.join(namespace_name, portion_name)
    files = list()

    for file in glob.glob(os.path.join(pgk_path, "**", "*.kv"), recursive=True):
        if os.path.isfile(file):
            files.append(os.path.relpath(file, pgk_path))

    for resource_folder in ('img', 'loc', 'snd'):
        for file in glob.glob(os.path.join(pgk_path, resource_folder, "**", "*"), recursive=True):
            if os.path.isfile(file):
                files.append(os.path.relpath(file, pgk_path))

    return files


def load_requirements(nev: NevType, path: str):
    """ load requirements from the available *requirements.txt file(s) of this namespace project.

    :param nev:                 dict of namespace environment variables.
                                required keys: REQ_FILE_NAME, REQ_TEST_FILE_NAME, namespace_name, package_name.
    :param path:                folder from where to load the *requirements.txt files.
    """
    namespace_name = nev_str(nev, 'namespace_name')
    dev_require: List[str] = list()
    req_file = os.path.join(path, nev_str(nev, 'REQ_FILE_NAME'))
    if os.path.exists(req_file):
        dev_require.extend(_ for _ in file_content(req_file).strip().split('\n') if _ and not _.startswith('#'))
    if 'de_setup' not in dev_require and nev_str(nev, 'package_name') != 'de_setup':
        dev_require.append('de_setup')

    nev['docs_require'] = [_ for _ in dev_require if _.startswith('sphinx_')]
    nev['install_require'] = [_ for _ in dev_require if _ and not _.startswith('sphinx_')]
    nev['portions_package_names'] = [_ for _ in dev_require if _.startswith(f'{namespace_name}_')]

    # if a namespace portion is needed for the setup process then use hyphen/dash instead of underscore
    # e.g. ae-literal instead of ae_literal (then ae.literal will be included in setup_require)
    nev['setup_require'] = [_ for _ in nev['install_require'] if not _.startswith(f'{namespace_name}_')]

    tests_require: List[str] = list()
    req_file = os.path.join(path, nev_str(nev, 'REQ_TEST_FILE_NAME'))
    if os.path.exists(req_file):
        tests_require.extend(_ for _ in file_content(req_file).strip().split('\n') if not _.startswith('#'))
    nev['tests_require'] = tests_require


def namespace_guess(root_path: str) -> str:
    """ guess name of namespace name from the package/app/project directory content.

    :param root_path:           optional rel/abs path to package/app/project root (def=current working directory).
    :return:                    name-prefix of the current folder.
    """
    return os.path.basename(root_path).split("_", maxsplit=1)[0]


def namespace_env_vars(namespace_name: str = "", root_path: str = "", old_nev: Optional[NevType] = None) -> NevType:
    """ analyse and map the development environment of a package/app/project into a dict of variables.

    :param namespace_name:      name of this namespace (def=value returned by :func:`namespace_guess`).
    :param root_path:           optional rel/abs path of package/app/project root (def=current working directory).
    :param old_nev              optional current namespace environment variables to be updated (and returned).
    :return:                    namespace environment variables/info dict (:paramref:`~namespace_env_vars.old_nev`).
    """
    if isinstance(old_nev, dict):
        nev: NevType = old_nev
    else:
        nev = dict()
        for var_name, var_val in globals().items():
            if not var_name.startswith('_') and var_name not in nev and isinstance(var_val, str):
                nev[var_name] = var_val     # add string globals like PORTIONS_COMMON_DIR if not already added

    nev['root_path'] = root_path = os.path.abspath(root_path or os.getcwd())
    package_name = os.path.basename(root_path)
    if package_name == 'docs' and os.path.exists(os.path.join(root_path, 'conf.py')):  # called by RTDocs build
        setup_path = os.path.dirname(root_path)     # better than appending "/.."
        package_name = os.path.basename(setup_path)
    else:
        setup_path = root_path
    nev['setup_path'] = setup_path
    nev['package_name'] = package_name

    setup_file = os.path.join(setup_path, 'setup.py')
    if os.path.exists(setup_file):
        nev['setup_file'] = setup_file

    nev['namespace_name'] = nsn = (namespace_name or namespace_guess(setup_path))

    load_requirements(nev, setup_path)      # load info from all *requirements.txt files

    nev['project_license'] = "OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
    nev['pypi_root'] = "https://pypi.org/project"
    nev['repo_root'] = f"https://{nev_str(nev, 'REPO_CODE_DOMAIN')}/{nsn}{nev_str(nev, 'REPO_GROUP_SUFFIX')}"
    nev['repo_pages'] = f"https://{nsn}{nev_str(nev, 'REPO_GROUP_SUFFIX')}.{nev_str(nev, 'REPO_PAGES_DOMAIN')}"

    if os.path.exists(os.path.join(setup_path, 'buildozer.spec')):  # ae.base.BUILD_CONFIG_FILE
        nev['project_type'] = APP_PRJ
        update_app_env_vars(nev)
    else:
        portion_root_path = os.path.join(setup_path, nsn)
        portion_type, portion_name, portion_modules = portion_type_name_modules(portion_root_path)
        if portion_type:
            nev['project_type'] = portion_type
            nev['portion_name'] = portion_name
            nev['portion_modules'] = portion_modules
            update_portion_env_vars(nev)
        elif 'setup_file' in nev:
            nev['project_type'] = ROOT_PRJ
            update_root_env_vars(nev)
        else:
            nev['project_type'] = ''

    return nev


def nev_str(nev: NevType, var_name: str) -> str:
    """ determine string value of namespace environment variable from passed nev or use default.

    AssertionError will be raised if the specified variable value is not of type str. use :func:`nev_val` instead.

    :param nev:                 namespace environment variables.
    :param var_name:            name of variable.
    :return:                    variable value or if not exists in nev then the constant/default value of this module or
                                an empty string. AssertionError will be raised if the specified variable value is not of
                                type str. Use :func:`nev_val` instead.
    """
    val = nev_val(nev, var_name)
    assert isinstance(val, str), f"nev_str() returns environment variable values of type string, got {type(val)}"
    return val


def nev_val(nev: NevType, var_name: str) -> NevVarType:
    """ determine value of namespace environment variable from passed nev or use default.

    :param nev:                 namespace environment variables.
    :param var_name:            name of variable.
    :return:                    variable value or if not exists in nev then the constant/default value of this module or
                                empty-string/"".
    """
    return nev.get(var_name, globals().get(var_name, ""))


def portion_type_name_modules(portion_root_path: str) -> Tuple[str, str, Tuple[str, ...]]:
    """ determine portion type, name and optional portion modules (if portion is a sub-package).

    :param portion_root_path:   file path of the root of the namespace portions.
    :return:                    the portion type, the portion name/placeholder and a tuple of package module names.
    """
    if not os.path.exists(portion_root_path):
        # return placeholders if not run/imported by portion repository (e.g. imported by namespace root repo)
        return "", "{portion-name}", ()

    # first search for single module portion
    files = glob.glob(os.path.join(portion_root_path, '*' + PY_EXT))
    if len(files) == 1:
        return MODULE_PRJ, os.path.basename(files[0])[:-len(PY_EXT)], ()
    if len(files) > 1:
        raise RuntimeError(f"More than one portion module found: {files}")

    # must be a sub-package (with optional extra modules)
    files = glob.glob(os.path.join(portion_root_path, '*', '*' + PY_EXT))
    if len(files) == 0:
        raise RuntimeError(f"Neither {MODULE_PRJ} nor {PACKAGE_PRJ} found in package path {portion_root_path}")
    extra_modules = list()
    name = ""
    for file_path in files:
        path, mod = os.path.split(file_path)
        if mod == '__init__' + PY_EXT:
            name = os.path.basename(path)
        else:
            extra_modules.append(mod)
    return PACKAGE_PRJ, name, tuple(extra_mod[:-len(PY_EXT)] for extra_mod in extra_modules)


def sh_exec(command_line: str, extra_args: Sequence = (), console_input: str = "",
            lines_output: Optional[List[str]] = None, app: Optional[Any] = None) -> int:
    """ execute command in the current working directory of the OS console/shell.

    :param command_line:        command line string to execute on the console/shell. could contain command line args
                                separated by whitespace characters (alternatively use :paramref:`~sh_exec.extra_args`).
    :param extra_args:          optional sequence of extra command line arguments.
    :param console_input:       optional string to be sent to the stdin stream of the console/shell.
    :param lines_output:        optional list to return the lines printed to stdout/stderr on execution.
    :param app:                 optional :class:`~ae.console.ConsoleApp` instance, only used for logging.
    :return:                    return code of the executed command or 999 if execution raised any other exception.
    """
    args = command_line.split() + list(extra_args)
    print_out = app.po if app else print
    print_out(f".     executing at {os.getcwd()}: {args}")
    pipe = None if lines_output is None else subprocess.PIPE
    run_result: Union[subprocess.CompletedProcess, subprocess.CalledProcessError]   # having: stdout/stderr/returncode
    try:
        run_result = subprocess.run(args, stdout=pipe, stderr=pipe, input=console_input.encode(), check=True)
    except subprocess.CalledProcessError as ex:                                             # pragma: no cover
        print_out(f"****  subprocess.run({args}) returned non-zero exit code {ex.returncode}; exception={ex}")
        run_result = ex
    except Exception as ex:
        print_out(f"****  subprocess.run({args}) raised exception {ex}")
        return 999

    if lines_output is not None:
        if run_result.stdout:
            lines_output.extend([line for line in run_result.stdout.decode().split('\n') if line])
        if run_result.stderr:
            lines_output.append(f"STDERR={run_result.stderr.decode()}")
    return run_result.returncode


def update_app_env_vars(nev: NevType):
    """ update dev env for an app project. """
    nev['root_version'] = code_file_version('main.py')
    nev['project_desc'] = code_file_title('main.py')


def update_portion_env_vars(nev: NevType):
    """ update dev env with portion project info. """
    namespace_name = nev_str(nev, 'namespace_name')      # ==cast(str, nev['namespace_name']) for mypy
    portion_name = nev_str(nev, 'portion_name')
    portion_type = nev['project_type']
    setup_path = nev_str(nev, 'setup_path')
    portion_root_path = os.path.join(setup_path, namespace_name)

    nev['portion_file_name'] = portion_file_name = \
        portion_name + (os.path.sep + '__init__.py' if portion_type == PACKAGE_PRJ else nev_str(nev, 'PY_EXT'))
    nev['portion_file_path'] = por_path = os.path.abspath(os.path.join(portion_root_path, portion_file_name))

    nev['find_packages_include'] = [namespace_name + (".*" if portion_type == PACKAGE_PRJ else "")]
    nev['import_name'] = f"{namespace_name}.{portion_name}"
    nev['package_resources'] = find_package_data(namespace_name, portion_name)
    nev['package_version'] = code_file_version(por_path)
    nev['pip_name'] = f"{namespace_name}-{portion_name.replace('_', '-')}"
    nev['portion_pypi_root'] = f"{nev['pypi_root']}/{nev['pip_name']}"
    nev['project_desc'] = f"{namespace_name} namespace portion {portion_name}: {code_file_title(por_path)}"


def update_root_env_vars(nev: NevType):
    """ update dev env with info of the namespace root project. """
    namespace_name = nev_str(nev, 'namespace_name')     # ==cast(str, nev['namespace_name']) for mypy
    namespace_len = len(namespace_name)
    setup_path = nev_str(nev, 'setup_path')

    nev['project_desc'] = f"{namespace_name} namespace root package: {code_file_title(nev_str(nev, 'setup_file'))}"
    nev['root_version'] = code_file_version(nev_str(nev, 'setup_file'))

    pypi_refs = list()
    pypi_refs_md = list()
    idx_names = list()
    for p_name in nev['portions_package_names']:
        pypi_refs.append(f'* `{p_name} <{nev["pypi_root"]}/{p_name}>`_')
        pypi_refs_md.append(f'* [{p_name}]({nev["pypi_root"]}/{p_name} "{namespace_name} namespace portion {p_name}")')
        dot_name = p_name[:namespace_len] + '.' + p_name[namespace_len + 1:]
        idx_names.append(dot_name)
        _pt, _pn, extra_modules = portion_type_name_modules(os.path.join(setup_path, '..', p_name, namespace_name))
        for e_mod in extra_modules:
            idx_names.append(dot_name + '.' + e_mod)
    nev['portions_pypi_refs'] = "\n".join(pypi_refs)                    # de_tpl_README.rst in namespace root package
    nev['portions_pypi_refs_md'] = "\n".join(pypi_refs_md)              # de_tpl_README.md in namespace root package
    nev['portions_import_names'] = ("\n" + " " * 4).join(idx_names)     # docs/de_tpl_index.rst in namespace root pkg
