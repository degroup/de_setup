""" setup  TEMPORARY FOR FIRST RELEASE TO PYPI - WILL FINALLY BE MAINTAINED BY NAMESPACE ROOT PROJECT..... """
import pprint
import setuptools

import de.setup
from de.setup import file_content, namespace_env_vars


if __name__ == "__main__":
    de.setup.REPO_GROUP_SUFFIX = 'group'
    nev = namespace_env_vars()
    package_name = nev['package_name']
    # kwargs that are optional for native/implicit namespace packages:
    # - namespace_packages=[namespace_name],
    # - packages=setuptools.find_packages(),
    setup_kwargs = dict(
        name=package_name,              # or pip install name (nev['pip_name']) or import name (nev['import_name'])
        version=nev['package_version'],
        author="Andi Ecker",
        author_email="aecker2@gmail.com",
        description=nev['project_desc'],
        license=nev['project_license'],
        long_description=file_content("README.md"),
        long_description_content_type="text/markdown",
        url=f"{nev['repo_root']}/{package_name}",
        packages=setuptools.find_namespace_packages(include=nev['find_packages_include']),
        # include_package_data=True,    # uncommenting this line results in NOT including package resources into sdist
        package_data={'': nev['package_resources']},
        zip_safe=not bool(nev['package_resources']),    # resources have to be available as separate files
        python_requires=">=3.6",
        install_requires=nev['install_require'],
        setup_requires=nev['setup_require'],
        extras_require={
            'docs': nev['docs_require'],
            'tests': nev['tests_require'],
            'dev': nev['docs_require'] + nev['tests_require'],
        },
        classifiers=[
            "Development Status :: 3 - Alpha",
            "License :: " + nev['project_license'],
            "Natural Language :: English",
            "Operating System :: OS Independent",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.6",
            "Topic :: Software Development :: Libraries :: Development Tools",
        ],
        keywords=[
            'configuration',
            'development',
            'environment',
            'productivity',
        ]
    )

    print("#  EXECUTING SETUPTOOLS SETUP #################################")
    print(pprint.pformat(setup_kwargs, indent=3, width=75, compact=True))
    setuptools.setup(**setup_kwargs)
    print("#  FINISHED SETUPTOOLS SETUP  #################################")
